﻿using ControlShop.WinService;
using ControlShop.WinService.Data;
using ControlShop.WinService.Entidades;
using System;
using System.Collections.Generic;
using System.IO;

namespace ControlShop.APP
{
    public class WebService
    {
        static ControlShopDataContext _ctx = new ControlShopDataContext();

        public static string EnviarArquivo(string TokenWebService, string ArquivoZip)
        {
            Log.Write(String.Format($"{DateTime.Now} : XML - Enviando WebService"));

            string Retorno = null;
            try
            {
                byte[] ZipByte = File.ReadAllBytes(ArquivoZip);
                string NomeArquivo = Path.GetFileName(ArquivoZip);

                var ws = new wsControlShop.CorporateSoapClient();

                Retorno = ws.RecebePacoteNotas(TokenWebService, NomeArquivo, ZipByte);
            }
            catch (Exception ex)
            {
                Log.Write(String.Format($"{DateTime.Now} : \n ========== ERRO FATAL ========== \n {ex.ToString()}"));
            }

            return Retorno;
        }

        public static void SalvarRetorno(int EstabelecimentoId, int QtdeXml, string ArquivoZip, string Retorno)
        {
            Log.Write(String.Format($"{DateTime.Now} : WebService - Salvando Retorno"));

            try
            {
                LJ_CONTROLSHOP_CONTROLE LjControlShopControle = new LJ_CONTROLSHOP_CONTROLE
                {
                    ESTABELECIMENTOID = EstabelecimentoId,
                    DATA_INICIO = DateTime.Today.AddDays(-1),
                    DATA_FINAL = DateTime.Today.AddDays(-1),
                    QTDE_XML = QtdeXml,
                    CADASTRADO_POR = "INTEGRACAO",
                    DATA_CADASTRO = DateTime.Now,
                    ALTERADO_POR = "INTEGRACAO",
                    DATA_ALTERACAO = DateTime.Now,
                    LJ_CONTROLSHOP_RETORNOs = new List<LJ_CONTROLSHOP_RETORNO>()
                };
                LJ_CONTROLSHOP_RETORNO LjControlShopRetorno = new LJ_CONTROLSHOP_RETORNO
                {
                    CADASTRADO_POR = "INTEGRACAO",
                    DATA_CADASTRO = DateTime.Now,
                    ALTERADO_POR = "INTEGRACAO",
                    DATA_ALTERACAO = DateTime.Now
                };

                if (string.IsNullOrEmpty(Retorno))
                    Retorno = "Retorno Vazio";

                if (Retorno == "Aquivo recebido e descompactado com sucesso !")
                {
                    if (!File.Exists(@"C:\inetpub\wwwroot\Portal\Uploads\ControlShop\Packages\" + Path.GetFileName(ArquivoZip)))
                    {
                        File.Move(ArquivoZip, @"C:\inetpub\wwwroot\Portal\Uploads\ControlShop\Packages\" + Path.GetFileName(ArquivoZip));
                    }
                    else
                    {
                        File.Delete(@"C:\inetpub\wwwroot\Portal\Uploads\ControlShop\Packages\" + Path.GetFileName(ArquivoZip));
                        File.Move(ArquivoZip, @"C:\inetpub\wwwroot\Portal\Uploads\ControlShop\Packages\" + Path.GetFileName(ArquivoZip));
                    }

                    LjControlShopControle.ARQUIVO = "/Uploads/ControlShop/Packages/" + Path.GetFileName(ArquivoZip);
                    LjControlShopRetorno.STATUS_RETORNO = "Arquivo recebido e descompactado com sucesso !";

                    LjControlShopControle.LJ_CONTROLSHOP_RETORNOs.Add(LjControlShopRetorno);

                    _ctx.Set<LJ_CONTROLSHOP_CONTROLE>().Add(LjControlShopControle);
                    _ctx.SaveChanges();
                }
                else
                {
                    if (!File.Exists(@"C:\inetpub\wwwroot\Portal\Uploads\ControlShop\Error\" + Path.GetFileName(ArquivoZip)))
                    {
                        File.Move(ArquivoZip, @"C:\inetpub\wwwroot\Portal\Uploads\ControlShop\Error\" + Path.GetFileName(ArquivoZip));
                    }
                    else
                    {
                        File.Delete(@"C:\inetpub\wwwroot\Portal\Uploads\ControlShop\Error\" + Path.GetFileName(ArquivoZip));
                        File.Move(ArquivoZip, @"C:\inetpub\wwwroot\Portal\Uploads\ControlShop\Error\" + Path.GetFileName(ArquivoZip));
                    }

                    LjControlShopControle.ARQUIVO = "/Uploads/ControlShop/Error/" + Path.GetFileName(ArquivoZip);
                    LjControlShopRetorno.STATUS_RETORNO = Retorno;

                    LjControlShopControle.LJ_CONTROLSHOP_RETORNOs.Add(LjControlShopRetorno);

                    _ctx.Set<LJ_CONTROLSHOP_CONTROLE>().Add(LjControlShopControle);
                    _ctx.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Log.Write(String.Format($"{DateTime.Now} : \n ========== ERRO FATAL ========== \n {ex.ToString()}"));
            }
        }
    }
}
