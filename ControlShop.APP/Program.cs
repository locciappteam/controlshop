﻿using ControlShop.WinService;
using ControlShop.WinService.Data;
using ControlShop.WinService.Entidades;
using ControlShop.WinService.Executores;
using System;
using System.Collections.Generic;
using System.Linq;


namespace ControlShop.APP
{
    class Program
    {
        static void Main(string[] args)
        {

            ControlShopDataContext _ctx = new ControlShopDataContext();

            Log.Write(String.Format($"{DateTime.Now} : Iniciando a tarefa..."));

            Console.WriteLine("-- INDICAR O PERIODO");

            
            Console.WriteLine("Data inicial (AAAA-MM-DD):");
            DateTime dtInicial = Convert.ToDateTime(Console.ReadLine());

            Console.WriteLine("Data Final (AAAA-MM-DD)");
            DateTime dtFinal = Convert.ToDateTime(Console.ReadLine());
            
           //DateTime dtInicial = Convert.ToDateTime("2020-03-09") ;
           //DateTime dtFinal = Convert.ToDateTime("2020-11-10");


            try
            {
                Log.Write(String.Format($"{DateTime.Now} : ControlShop - Obtendo Tokens..."));
                List<LJ_CONTROLSHOP_TOKEN> TokensControlShop = _ctx.Set<LJ_CONTROLSHOP_TOKEN>().Where(p=> p.ESTABELECIMENTOID == 272).ToList();

                foreach (var ControlShop in TokensControlShop)
                {
                    Log.Write(String.Format($"{DateTime.Now} : {ControlShop.ESTABELECIMENTO.DESC_ESTAB}"));

                    string Path = @"\\amsaoawsp021\wwwroot\Portal\Uploads\ControlShop\XMLs\";
                    Folder.Create(Path);

                    //var ListaXml = ArquivoXML.Obter(DateTime.Today.AddDays(-1), DateTime.Today.AddDays(-1), ControlShop.ESTABELECIMENTOID);

                    var ListaXml = ArquivoXML.Obter(dtInicial, dtFinal, ControlShop.ESTABELECIMENTOID);


                    if (ListaXml.Count != 0)
                    {
                        ArquivoXML.Gerar(ListaXml, Path);

                        string ArquivoZip = ArquivoXML.Zipar(ListaXml, Path, ControlShop.ESTABELECIMENTO.CNPJ);

                        string Retorno = WebServiceControlShop.EnviarArquivo(ControlShop.TOKEN_WEBSERVICE, ArquivoZip);

                        WebServiceControlShop.SalvarRetorno(ControlShop.ESTABELECIMENTOID, ListaXml.Count(), ArquivoZip, Retorno);

                    }

                }
            }
            catch (Exception ex)
            {
                Log.Write(String.Format($"{DateTime.Now} : \n ========== ERRO FATAL ========== \n {ex.ToString()}"));
            }

            Log.Write(String.Format($"{DateTime.Now} : Fim da tarefa..."));
        }
    }
}
