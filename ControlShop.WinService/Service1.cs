﻿using ControlShop.WinService.Executores;
using System;
using System.ServiceProcess;

namespace ControlShop.WinService
{
    public partial class Service1 : ServiceBase
    {
        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            Log.Write(strMensagem: String.Format($"{DateTime.Now} : Serviço Iniciado!"), Tipo: 0);

            TimerControlShop.TimeScheduler();
        }

        protected override void OnStop()
        {
            Log.Write(String.Format($"{DateTime.Now} : Serviço Paralisado!"));
        }
    }
}
