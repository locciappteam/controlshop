﻿using ControlShop.WinService.Data;
using ControlShop.WinService.Entidades;
using ControlShop.WinService.Executores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Timers;

namespace ControlShop.WinService.Executores
{
    public class TimerControlShop
    {
        static Timer timer;
        static ControlShopDataContext _ctx = new ControlShopDataContext();

        public static void TimeScheduler()
        {
            Log.Write(String.Format($"{DateTime.Now} : Realizando agendamento..."));

            DateTime ScheduledTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 6, 0, 0, 0);
            if (DateTime.Now > ScheduledTime)
            {
                ScheduledTime = ScheduledTime.AddDays(1);
            }
            Log.Write(String.Format($"{DateTime.Now} : Tarefa agendada para {ScheduledTime}"));

            double TickTime = (double)(ScheduledTime - DateTime.Now).TotalMilliseconds;
            timer = new Timer(TickTime);
            timer.Elapsed += new ElapsedEventHandler(TimeElapsed);
            timer.Start();
        }

        private static void TimeElapsed(object sender, ElapsedEventArgs e)
        {
            timer.Stop();
            Log.Write(String.Format($"{DateTime.Now} : Iniciando a tarefa..."));
            
            try
            {
                Log.Write(String.Format($"{DateTime.Now} : ControlShop - Obtendo Tokens..."));
                List<LJ_CONTROLSHOP_TOKEN> TokensControlShop = _ctx.Set<LJ_CONTROLSHOP_TOKEN>().ToList();

                foreach (var ControlShop in TokensControlShop)
                {
                    Log.Write(String.Format($"{DateTime.Now} : {ControlShop.ESTABELECIMENTO.DESC_ESTAB}"));

                    string Path = @"C:\inetpub\wwwroot\Portal\Uploads\ControlShop\XMLs\";
                    Folder.Create(Path);

                    var ListaXml = ArquivoXML.Obter(DateTime.Today.AddDays(-1), DateTime.Today.AddDays(-1), ControlShop.ESTABELECIMENTOID);
                    
                    ArquivoXML.Gerar(ListaXml, Path);

                    string ArquivoZip = ArquivoXML.Zipar(ListaXml, Path, ControlShop.ESTABELECIMENTO.CNPJ);

                    string Retorno = WebServiceControlShop.EnviarArquivo(ControlShop.TOKEN_WEBSERVICE, ArquivoZip);

                    WebServiceControlShop.SalvarRetorno(ControlShop.ESTABELECIMENTOID, ListaXml.Count(), ArquivoZip, Retorno);
                }
            }
            catch (Exception ex)
            {
                Log.Write(String.Format($"{DateTime.Now} : \n ========== ERRO FATAL ========== \n {ex.ToString()}"));
            }

            Log.Write(String.Format($"{DateTime.Now} : Fim da tarefa..."));
            TimeScheduler();
        }
    }
}
