﻿using ControlShop.WinService.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace ControlShop.WinService.Executores
{
    public class ArquivoXML
    {
        private static ViewsDataContext _views = new ViewsDataContext();

        public static List<W_CONTROLSHOP_XML> Obter(DateTime DataInicial, DateTime DataFinal, int EstabelecimentoId)
        {
            Log.Write(String.Format($"{DateTime.Now} : XML - Obtendo arquivos"));

            _views.CommandTimeout = 0;
            List<W_CONTROLSHOP_XML> ListaXML = new List<W_CONTROLSHOP_XML>();
            try
            {
                ListaXML = (
                    from
                        a in _views.W_CONTROLSHOP_XMLs
                    where
                        a.DATA_VENDA >= DataInicial && a.DATA_VENDA <= DataFinal
                        && a.ESTABELECIMENTOID == EstabelecimentoId
                    select
                        a
                ).ToList();
            }
            catch (Exception ex)
            {
                Log.Write(String.Format($"{DateTime.Now} : \n ========== ERRO FATAL ========== \n {ex.ToString()}"));
            }

            return ListaXML;
        }

        public static void Gerar(List<W_CONTROLSHOP_XML> ListaXML, string Path)
        {
            Log.Write(String.Format($"{DateTime.Now} : XML - Gerando arquivos"));

            try
            {
                XmlDocument xmlDocument = new XmlDocument();
                foreach (var XML in ListaXML)
                {
                    xmlDocument.LoadXml(XML.ARQUIVO_XML.ToString());

                    var ChaveNFe = xmlDocument.SelectSingleNode("//@Id").Value;

                    xmlDocument.Save(Path + ChaveNFe + ".xml");
                }
            }
            catch (Exception ex)
            {
                Log.Write(String.Format($"{DateTime.Now} : \n ========== ERRO FATAL ========== \n {ex.ToString()}"));
            }
        }

        public static string Zipar(List<W_CONTROLSHOP_XML> ListaXML, string Path, string CNPJ)
        {
            Log.Write(String.Format($"{DateTime.Now} : XML - Zipando arquivos"));

            string ArquivoZip = String.Format($"{Path.Replace("XMLs\\", "")}{CNPJ}_{DateTime.Today.ToString("yyyyMMdd_HHmmss")}.zip");
            try
            {

                if (!File.Exists(ArquivoZip))
                {
                    ZipFile.CreateFromDirectory(
                        Path,
                        ArquivoZip
                    );
                }
                else
                {
                    File.Delete(ArquivoZip);
                    ZipFile.CreateFromDirectory(
                        Path,
                        ArquivoZip
                    );
                }
            }
            catch (Exception ex)
            {
                Log.Write(String.Format($"{DateTime.Now} : \n ========== ERRO FATAL ========== \n {ex.ToString()}"));
            }

            return ArquivoZip;
        }

        protected static void Dispose(bool disposing)
        {
            _views.Dispose();
        }
    }
}
