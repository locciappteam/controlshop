﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlShop.WinService.Executores
{
    public class Folder
    {
        public static void Create(string Path)
        {
            try { 
                Log.Write(String.Format($"{DateTime.Now} : XML - Criando pasta"));

                if (!Directory.Exists(Path))
                {
                    Directory.CreateDirectory(Path);
                }
                else
                {
                    Directory.Delete(Path, true);
                    Directory.CreateDirectory(Path);
                }
            }
            catch (Exception ex)
            {
                Log.Write(String.Format($"{DateTime.Now} : \n ========== ERRO FATAL ========== \n {ex.ToString()}"));
            }
        }
    }
}
