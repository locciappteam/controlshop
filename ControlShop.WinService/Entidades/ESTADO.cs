﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ControlShop.WinService.Entidades
{
    public class ESTADO
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public string UF { get; set; }
        public string NOME { get; set; }
        public string CAPITAL { get; set; }
        public string GENTILICO { get; set; }

        public virtual ICollection<ESTABELECIMENTO> ESTABELECIMENTOs { get; set; }
    }
}
