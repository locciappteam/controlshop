﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlShop.WinService.Entidades
{
    public class LJ_CONTROLSHOP_RETORNO
    {
        public LJ_CONTROLSHOP_RETORNO()
        {
            DATA_ALTERACAO = DateTime.Now;
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [ForeignKey("LJ_CONTROLSHOP_CONTROLE")]
        public int CONTROLEID { get; set; }

        public string STATUS_RETORNO { get; set; }
        public string CADASTRADO_POR { get; set; }
        public DateTime DATA_CADASTRO { get; set; }
        public string ALTERADO_POR { get; set; }
        public DateTime DATA_ALTERACAO { get; set; }

        public virtual LJ_CONTROLSHOP_CONTROLE LJ_CONTROLSHOP_CONTROLE { get; set; }
    }
}
