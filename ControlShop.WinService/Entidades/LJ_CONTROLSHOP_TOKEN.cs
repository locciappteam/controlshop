﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlShop.WinService.Entidades
{
    public class LJ_CONTROLSHOP_TOKEN
    {
        public LJ_CONTROLSHOP_TOKEN()
        {
            DATA_ALTERACAO = DateTime.Now;
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [ForeignKey("ESTABELECIMENTO")]
        public int ESTABELECIMENTOID { get; set; }

        public string TOKEN_WEBSERVICE { get; set; }
        public string CADASTRADO_POR { get; set; }
        public DateTime DATA_CADASTRO { get; set; }
        public string ALTERADO_POR { get; set; }
        public DateTime DATA_ALTERACAO { get; set; }

        public virtual ESTABELECIMENTO ESTABELECIMENTO { get; set; }
    }
}
