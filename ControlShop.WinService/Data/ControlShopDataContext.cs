﻿using ControlShop.WinService.Entidades;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlShop.WinService.Data { 
    public class ControlShopDataContext : DbContext
    {
        public ControlShopDataContext()
            : base(@"Data Source=AMSAOCBRDB01;Initial Catalog=PORTAL_LOCCITANE;Persist Security Info=True;User ID=portalloccitane;Password=3u3NO483")
        { }

        public DbSet<ESTADO> Estado { get; set; }
        public DbSet<ESTABELECIMENTO> Estabelecimento { get; set; } 
        public DbSet<LJ_CONTROLSHOP_CONTROLE> LjControlShopControle { get; set; }
        public DbSet<LJ_CONTROLSHOP_RETORNO> LjControlShopRetorno { get; set; }
        public DbSet<LJ_CONTROLSHOP_TOKEN> LjControlShopToken { get; set; }




        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}
